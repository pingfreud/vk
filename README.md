# README #

This is the vk application for the clinical assessment of speech measures. Current text data was taken from TED talks transcripts.

### What is this repository for? ###

* Prototyping of the first demo versions
* Version 0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies: Python 2.7; kivy 1.9.1; R package LSAfun; raplyzer (available on git;requires eSpeaker)
* Database configuration: download semantic space EN_100k (http://www.lingexp.uni-tuebingen.de/z2/LSAspaces/) to "~/LSASpace/EN_100k.rda"
* How to run tests: No tests; Genius code is free of bugs. Just kidding. Coming soon.
* Deployment instructions: run "python2.7 main.py"

### Contribution guidelines ###

* E-mail: felipe.c.argolo@hotmail.com

### Who do I talk to? ###

* ping_freud
